<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEstablishmentsResearchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('establishments_researchers', function(Blueprint $table)
		{
			$table->foreign('establishment_id', 'fk_establishments_researchers_establishments1')->references('id')->on('establishments')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('researcher_id', 'fk_establishments_researchers_researchers1')->references('id')->on('researchers')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('establishments_researchers', function(Blueprint $table)
		{
			$table->dropForeign('fk_establishments_researchers_establishments1');
			$table->dropForeign('fk_establishments_researchers_researchers1');
		});
	}

}
