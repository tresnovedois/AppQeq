<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstablishmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('establishments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 60);
			$table->text('description', 65535)->nullable();
			$table->string('national_id', 20);
			$table->string('url_logo', 100)->nullable();
			$table->string('email', 45)->unique('email_UNIQUE');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('establishments');
	}

}
