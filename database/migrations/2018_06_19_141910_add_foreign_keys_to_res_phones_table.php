<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToResPhonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('res_phones', function(Blueprint $table)
		{
			$table->foreign('researcher_id', 'fk_res_phones_researchers1')->references('id')->on('researchers')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('res_phones', function(Blueprint $table)
		{
			$table->dropForeign('fk_res_phones_researchers1');
		});
	}

}
