<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResearchersLotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('researchers_lots', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('researcher_id')->unsigned()->index('fk_researchers_lots_researchers1_idx');
			$table->integer('lot_id')->unsigned()->index('fk_researchers_lots_lots1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('researchers_lots');
	}

}
