<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEstPhonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('est_phones', function(Blueprint $table)
		{
			$table->foreign('establishment_id', 'fk_est_phones_establishments1')->references('id')->on('establishments')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('est_phones', function(Blueprint $table)
		{
			$table->dropForeign('fk_est_phones_establishments1');
		});
	}

}
