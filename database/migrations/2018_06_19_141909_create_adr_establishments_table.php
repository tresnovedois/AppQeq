<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdrEstablishmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adr_establishments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('address', 90);
			$table->string('postal_code', 20);
			$table->string('country', 45);
			$table->string('city', 50);
			$table->string('state', 2);
			$table->float('latitude', 10, 0)->nullable();
			$table->float('longitude', 10, 0)->nullable();
			$table->integer('establishment_id')->unsigned()->index('fk_adr_establishments_establishments1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adr_establishments');
	}

}
