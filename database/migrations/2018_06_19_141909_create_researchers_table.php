<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResearchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('researchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 60);
			$table->string('national_id', 20);
			$table->string('email', 45)->nullable()->unique('email_UNIQUE');
			$table->string('url_curriculum', 100)->nullable();
			$table->string('url_photo', 100)->nullable();
			$table->text('c_summary', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('researchers');
	}

}
