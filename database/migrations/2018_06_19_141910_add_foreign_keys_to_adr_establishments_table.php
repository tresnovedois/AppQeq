<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdrEstablishmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('adr_establishments', function(Blueprint $table)
		{
			$table->foreign('establishment_id', 'fk_adr_establishments_establishments1')->references('id')->on('establishments')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('adr_establishments', function(Blueprint $table)
		{
			$table->dropForeign('fk_adr_establishments_establishments1');
		});
	}

}
