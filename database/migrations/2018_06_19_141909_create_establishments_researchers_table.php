<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstablishmentsResearchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('establishments_researchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('researcher_id')->unsigned()->index('fk_establishments_researchers_researchers1');
			$table->integer('establishment_id')->unsigned()->index('fk_establishments_researchers_establishments1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('establishments_researchers');
	}

}
