<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdrResearchersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('adr_researchers', function(Blueprint $table)
		{
			$table->foreign('researcher_id', 'fk_adr_researchers_researchers1')->references('id')->on('researchers')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('adr_researchers', function(Blueprint $table)
		{
			$table->dropForeign('fk_adr_researchers_researchers1');
		});
	}

}
