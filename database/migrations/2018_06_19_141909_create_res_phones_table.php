<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResPhonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('res_phones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ddi');
			$table->integer('ddd');
			$table->integer('phone');
			$table->integer('researcher_id')->unsigned()->index('fk_res_phones_researchers1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('res_phones');
	}

}
