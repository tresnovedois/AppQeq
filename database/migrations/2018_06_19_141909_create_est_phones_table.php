<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstPhonesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('est_phones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ddi')->nullable();
			$table->integer('ddd')->nullable();
			$table->integer('phone')->nullable();
			$table->integer('establishment_id')->unsigned()->index('fk_est_phones_establishments1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('est_phones');
	}

}
