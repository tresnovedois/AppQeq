<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToResearchersLotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('researchers_lots', function(Blueprint $table)
		{
			$table->foreign('lot_id', 'fk_researchers_lots_lots1')->references('id')->on('lots')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('researcher_id', 'fk_researchers_lots_researchers1')->references('id')->on('researchers')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('researchers_lots', function(Blueprint $table)
		{
			$table->dropForeign('fk_researchers_lots_lots1');
			$table->dropForeign('fk_researchers_lots_researchers1');
		});
	}

}
