<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Establishment;
use App\Res_Phone;
use App\Adr_Researcher;
use App\Lot;
use DB;

class Researcher extends Model
{
    //use SoftDeletes;

    protected $table = 'researchers';
    protected $fillable = [
        'name',
        'national_id',
        'email',
        'url_curriculum',
        'url_photo',
        'c_summary'
    ];

    public function resPhones(){
        return $this->hasMany('App\Res_Phone');
    }

    public function adrResearchers(){
        return $this->hasMany('App\Adr_Researcher');
    }

    public function establishments(){
        return $this->belongsToMany('App\Establishment', 'establishments_researchers');
    }

    public function lots(){
        return $this->belongsToMany('App\Lot', 'researchers_lots');
    }
    
}
