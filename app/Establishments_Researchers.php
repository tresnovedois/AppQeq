<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Establishments;
use App\Researcher;
use DB;

class Establishments_Researchers extends Model
{
    protected $table = 'establishments_researchers';

    protected $fillable = [
        'researcher_id',
        'establishment_id'
    ];

    public $timestamps = false;
}
