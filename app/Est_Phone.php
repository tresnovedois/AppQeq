<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Establishment;

class Est_Phone extends Model
{
    protected $table = 'est_phones';
    protected $fillable = [
        'ddi',
        'ddd',
        'phone',
        'establishment_id'
    ];
    public $timestamps = false;

    public function establishments(){
        return $this->belongsTo('App\Establishment', 'establishments_id');
    }    
}
