<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adr_Researcher extends Model
{
    protected $table = 'adr_researchers';
    protected $fillable = [
        'address',
        'postal_code',
        'country',
        'city',
        'state',
        'researcher_id'
    ];
    
    public $timestamps = false;

    public function researcher(){
        return $this->belongsTo('App\Researcher','researchers_id');
    }
}
