<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Researcher;

class Lot extends Model
{
    protected $table = 'lots';

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;

    public function researchers(){
        return $this->belongsToMany('App\Researcher', 'researchers_lots');
    }
}
