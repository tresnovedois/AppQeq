<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Establishment;

class Adr_Establishment extends Model
{
    protected $table = 'adr_establishments';
    protected $fillable = [
        'address',
        'postal_code',
        'country',
        'city',
        'latitude',
        'longitude',
        'state',
        'establishment_id'
    ];
    public $timestamps = false;

    public function establishments(){
        return $this->belongsTo('App\Establishment', 'establishments_id');
    }
}
