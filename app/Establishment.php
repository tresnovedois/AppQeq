<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Researcher;
use App\Adr_Establishment;
use App\Res_Phone;
use App\Est_Phone;
use DB;

class Establishment extends Model
{

    //use SoftDeletes;

    protected $table = 'establishments';
    protected $fillable = [
        'name',
        'description',
        'national_id',
        'url_logo',
        'email'
    ];

    public function estPhones(){
        return $this->hasMany('App\Est_Phone');
    }

    public function adrEstablishments(){
        return $this->hasMany('App\Adr_Establishment');
    }

    public function researchers(){
        return $this->belongsToMany('App\Researcher', 'establishments_researchers');
    }

}
