<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Establishment;
use App\Researcher;
use App\Lot;
use App\Establishments_Researchers as Er;
use DB;

class VisitorsController extends Controller
{
    public function homeVisitors()
    {  
        return view('visitors.home');
    }

    //RESEARCHERS

    public function indexResearchers()
    {
        $researchers = DB::table('researchers')
        ->orderBy('name', 'asc')->get();   

        return view('visitors.researchers.index', ['researchers'=> $researchers]);
    }

    public function showResearchers($id)
    {
        $researcher = Researcher::find($id); 
        return view('visitors.researchers.show', ['researcher'=> $researcher]);
    }

    //LOTS

    public function indexLots()
    {
        $lots = DB::table('lots')
        ->orderBy('name', 'asc')->get();   

        return view('visitors.lots.index', ['lots'=> $lots]);
    }

    public function showLots($id)
    {
        $lot = Lot::find($id); 
        return view('visitors.lots.show', ['lot'=> $lot]);
    }

    //ESTABLISHMENTS
    public function indexEstablishments()
    {
        $establishments = DB::table('establishments')
        ->orderBy('name', 'asc')->get();

        return view('visitors.establishments.index', ['establishments'=> $establishments]);
    }


    public function showEstablishments($id)
    {
        $establishment = Establishment::find($id);
        return view('visitors.establishments.show', ['establishment' => $establishment]);
    }
}