<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Researcher;
use App\Establishment;
use App\Res_Phone as Phone;
use App\Adr_Researcher as Adr;
use App\Establishments_Researchers as Er;
use App\Researchers_Lots as Rl;
use App\ImageController;
use App\Lot;
use Auth;
use DB;

class ResearchersController extends Controller
{
    //SHOW//

    public function index()
    {
        $researchers = DB::table('researchers')
        ->orderBy('name', 'asc')->get();        

         return view('researchers.index', ['researchers'=> $researchers]);
    }

    public function show($id)
    {
        $researcher = Researcher::find($id); 
        $establishments = Establishment::all();
        $lots = Lot::all();
        $rl = Rl::where('researcher_id','=', $id)->get();
        $er = Er::where('researcher_id','=', $id)->get();

        return view('researchers.show', ['researcher'=> $researcher, 'establishments'=> $establishments, 'er'=> $er, 'lots'=> $lots, 'rl'=> $rl]);
    }

    public function showEditAddress($id)
    {
      $adr = Adr::find($id);
      return view('researchers.editaddress', ['adr'=> $adr]);
    }

    //CREATES//

    public function link(Request $request)
    {   
        if(Establishment::find($request->input('establishmentId')))
        {
        Er::create([
            'researcher_id' => $request->input('id'),
            'establishment_id' => $request->input('establishmentId')
        ]);
        }
        
        return back();
    }

    public function toLot(Request $request)
    {   
        if(Lot::find($request->input('lotId')))
        {
        Rl::create([
            'researcher_id' => $request->input('id'),
            'lot_id' => $request->input('lotId')
        ]);
        }
        
        return back();
    }


    public function newPhone(Request $request)
    {
        Phone::create([
         'researcher_id' => $request->input('id'),
         'ddi' => $request->input('ddi'),
         'ddd' => $request->input('ddd'),
         'phone' => $request->input('phone')
        ]);
 
        return back();
    }

    public function newAddr(Request $request)
    {
       Adr::create([
        'researcher_id' => $request->input('id'),
        'address' => $request->input('address'),
        'postal_code' => $request->input('postal_code'),
        'city' => $request->input('city'),
        'country' => $request->input('country'),
        'state' => $request->input('state')
        ]);

        return back();
    }   

    public function create(Researcher $researcher)
    {   
        return view('researchers.create', ['researcher'=> $researcher]);
    }

    private function registerPhone(Request $request, $researcherId)
    {
        return Phone::create([
            'ddi' => $request->input('ddi'),
            'ddd' => $request->input('ddd'),
            'phone' => $request->input('phone'),
            'researcher_id' => $researcherId
        ]);
    }

    private function registerAddress(Request $request, $researcherId)
    {
        return Adr::create([
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'country' => $request->input('country'),
            'state' => $request->input('state'),
            'researcher_id' => $researcherId
        ]);
    }

    public function store(Request $request)
    {
        if (Auth::check()){
            $researcher = Researcher::create([
                'name' => $request->input('name'),
                'national_id' => $request->input('national_id'),
                'email' => $request->input('email'),
                'url_curriculum' => $request->input('url_curriculum'),
                'url_photo' => $request->input('url_photo'),
                'c_summary' => $request->input('c_summary'),
                'user_id' => Auth::user()->id
            ]);

        $this->registerAddress($request, $researcher->id);
        $this->registerPhone($request, $researcher->id);
            
        if($researcher){
            return redirect()->route('researchers.show', ['researcher' => $researcher->id]);
            }
    }
        
    return back()->withInput()->with('error' , 'Algo deu errado :(');
    }

   //SAVES//

    public function edit($id)
    {
       $researcher = Researcher::find($id);       
       return view('researchers.edit', ['researcher'=> $researcher]);
    }

    public function update(Request $request, $id)
    {
        $reseacherUpdate = Researcher::where('id', $id)
        ->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'national_id' => $request->input('national_id'),
            'c_summary' => $request->input('c_summary'),
            'url_curriculum' => $request->input('url_curriculum')

        ]);

        if($reseacherUpdate){
            return redirect()->route('researchers.show', ['researcher'=>$id]);
        }
        return back()->withInput();
    }

    public function editAddress(Request $request, $id)
    {
        $a_update = Adr::where('id', $id)
        ->update([
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'country' => $request->input('country'),
            'state' => $request->input('state')
        ]);
        $researcher = $request->input('researcher_id');
        return redirect()->route('researchers.show' , ['researcher' => $researcher]);
    }

    //DELETES//

   public function destroy($id)
   {
       $find = Researcher::find($id);
       if($find->delete())
       {
            return redirect()->route('researchers.index')
            ->with('success', 'Algo deu certo!');
       }
       return back()->withInput()->with('error', 'Algo deu errado... pesquisador não removido!');
   }

    public function deletePhone(Request $request)
    {
        Phone::destroy([
            'id' => $request->input('id')
        ]);
        
        return back();
    }

    public function deleteAddress(Request $request)
    {
        Adr::destroy([
            'id' => $request->input('id')
        ]);
        
        return back();
    }

    public function unlink(Request $request)
    {
        Er::destroy([
            'id' => $request->input('id')
        ]);
        
        return back();
    }

    public function deleteLot(Request $request)
    {
        Rl::destroy([
            'id' => $request->input('id')
        ]);
        
        return back();
    }

    public function deletePhoto(Request $request)
    {
        $id = $request->input('id');
        $deletep = Researcher::where('id', $id)
        ->update(['url_photo' => $request->input('url_photo')]);
        
        return back();
    }

}