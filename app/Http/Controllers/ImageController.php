<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Researcher;
use App\Establishment;
use DB;
use Image;

class ImageController extends Controller

{
    public function imagePostEstablishments(Request $request)
    {
	    $this->validate($request, [
	    	//'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $destinationPath = public_path('/images/establishments');
        
        $image = $request->file('image');

        $img = Image::make($image->getRealPath());

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();   

        $image->move($destinationPath, $input['imagename']);
        //passar o id do researcher
        $id = $request->input('id');
        
        Establishment::where('id', $id)->update(['url_logo' => '/images/establishments/' . $input['imagename']]);

        return back()->with('success','Image Upload successful')->with('imagename',$input['imagename']);
    }

    public function imagePostResearchers(Request $request)
    {
	    $this->validate($request, [
          //'title' => 'required',
          'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $destinationPath = public_path('/images/researchers');
        
        $image = $request->file('image');

        $img = Image::make($image->getRealPath());

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();   

        $image->move($destinationPath, $input['imagename']);

        //passar o id do researcher
        $id = $request->input('id');
        
        Researcher::where('id', $id)->update(['url_photo' => '/images/researchers/' . $input['imagename']]);

        return back()->with('success','Image Upload successful')->with('imagename',$input['imagename']);
    }
}

    //return back()->withInput()->with('error' , 'Algo deu errado :(');