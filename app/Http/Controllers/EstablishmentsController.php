<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Establishment;
use App\Est_Phone;
use App\Adr_Establishment;
use App\Res_Phone;
use App\Researcher;
use DB;
use Auth;

class EstablishmentsController extends Controller
{
    //SHOW//

    public function index()
    {
      $establishments = DB::table('establishments')
      ->orderBy('name', 'asc')->get();  
      
      return view('establishments.index', ['establishments'=> $establishments]);
    }

    public function show(Establishment $establishment)
    {
       $establishemnt = Establishment::find($establishment->id);

       return view('establishments.show', ['establishment' => $establishment]);
    }

    public function showEditAddress($id)
    {
      $adr = Adr_Establishment::find($id);
      return view('establishments.editaddress', ['adr'=> $adr]);
    }
    
    //CREATES//

    public function newPhone(Request $request){

         Est_Phone::create([
         'establishment_id' => $request->input('id'),
         'ddi' => $request->input('ddi'),
         'ddd' => $request->input('ddd'),
         'phone' => $request->input('phone')
        ]);

        return back();
    }

    public function newAddr(Request $request)
    {
        Adr_Establishment::create([
        'establishment_id' => $request->input('id'),
        'latitude' => $request->input('latitude'),
        'longitude' => $request->input('longitude'),
        'address' => $request->input('address'),
        'postal_code' => $request->input('postal_code'),
        'city' => $request->input('city'),
        'country' => $request->input('country'),
        'state' => $request->input('state')
        ]);

        return back();
    } 

    public function create(Establishment $establishment)
    {
        return view('establishments.create', ['establishment'=> $establishment]);
    }

    private function registerPhone(Request $request, $establishmentId)
    {
        return Est_Phone::create([
        'ddi' => $request->input('ddi'),
        'ddd' => $request->input('ddd'),
        'phone' => $request->input('phone'),
        'establishment_id' => $establishmentId
        ]);
    }

    private function registerAddress(Request $request, $establishmentId)
    {
        return Adr_Establishment::create([
        'address' => $request->input('address'),
        'postal_code' => $request->input('postal_code'),
        'city' => $request->input('city'),
        'country' => $request->input('country'),
        'state' => $request->input('state'),
        'latitude' => $request->input('latitude'),
        'longitude' => $request->input('longitude'),
        'establishment_id' => $establishmentId
        ]);
    }

    //SAVES//

    public function store(Request $request)
    {
        if (Auth::check()){
            $establishment = Establishment::create([
            'name' => $request->input('name'),
            'national_id' => $request->input('national_id'),
            'email' => $request->input('email'),
            'description' => $request->input('description'),
            'url_logo' => $request->input('url_logo'),
            'user_id' => Auth::user()->id
            ]);

        $this->registerAddress($request, $establishment->id);
        $this->registerPhone($request, $establishment->id);
            
        if($establishment){
            return redirect()->route('establishments.show', ['establishment' => $establishment->id])
            ->with('success' , 'Instituição criada com sucesso!');
        }
    }
        
    return back()->withInput()->with('error' , 'Algo deu errado :(');
    }

    public function edit($id)
    {
       $establishment = Establishment::find($id);       
       return view('establishments.edit', ['establishment'=> $establishment]);
    }

    public function update(Request $request, $id)
    {
        $establishmentUpdate = Establishment::where('id', $id)
        ->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'national_id' => $request->input('national_id'),
            'description' => $request->input('description')
        ]);

        if($establishmentUpdate){
            return redirect()->route('establishments.show', ['establishment'=>$id]);
        }
        return back()->withInput();
    }

    public function editAddress(Request $request, $id)
    {
        $a_update = Adr_Establishment::where('id', $id)
        ->update([
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'country' => $request->input('country'),
            'state' => $request->input('state'),
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude')
        ]);
        $establishment = $request->input('establishment_id');
        return redirect()->route('establishments.show' , ['establishment' => $establishment]);
    }
    //DELETES//

    public function destroy($id)
    {
        $find = Establishment::find($id);
        if($find->delete())
        {
            return redirect()->route('establishments.index')
            ->with('success', 'Algo deu certo... instituição removida!');
        }

        return back()->withInput()->with('error', 'Algo deu erado... instituição não removido!');
    }

    public function deletePhone(Request $request)
    {
        Est_Phone::destroy([
            'id' => $request->input('id')
        ]);
        return back();
    }

    public function deleteAddress(Request $request)
    {
        Adr_Establishment::destroy([
            'id' => $request->input('id')
        ]);
        return back();
    }

    public function deletePhoto(Request $request)
    {
        $id = $request->input('id');
        $deletep = Establishment::where('id', $id)
        ->update(['url_logo' => $request->input('url_logo')]);
        
        return back();
    }

    //GMAPS

    public function showMap(Establishment $establishment, $id)
    {
        $locations = DB::table('adr_establishments')->where('id', '=', $id)->get();
        $establishment = DB::table('establishments')->where('id', '=', $locations[0]->establishment_id)->get();
 
        return view('showmap', ['locations' => $locations, 'establishment' => $establishment]);       
    }

    public function showMapVisitor(Establishment $establishment, $id)
    {
        $locations = DB::table('adr_establishments')->where('id', '=', $id)->get();
        $establishment = DB::table('establishments')->where('id', '=', $locations[0]->establishment_id)->get();
 
        return view('visitors.showmap', ['locations' => $locations, 'establishment' => $establishment]);       
    }
    
}