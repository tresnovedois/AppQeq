<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lot;
use App\Researcher;
use App\Researchers_Lots as Rl;
use Auth;
use DB;

class LotsController extends Controller
{
    public function index()
    {
        $lots = DB::table('lots')
        ->orderBy('name', 'asc')->get();   

         return view('lots.index', ['lots'=> $lots]);
    }

    public function show($id)
    {
        $lot = Lot::find($id); 
        $researchers = Researcher::all();
        $rl = Rl::where('researcher_id','=', $id)->get();

        return view('lots.show', ['lot'=> $lot, 'researchers'=> $researchers, 'rl'=> $rl]);
    }

    public function create(Lot $lot)
    {   
        return view('lots.create', ['lot'=> $lot]);
    }

    public function store(Request $request)
    {
        if (Auth::check()){
            $lot = Lot::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'user_id' => Auth::user()->id
            ]);
            
        if($lot){
            return redirect()->route('lots.show', ['lot' => $lot->id]);
            }
    }
        
    return back();
    }

    public function edit($id)
    {
       $lot = Lot::find($id);       
       return view('lots.edit', ['lot'=> $lot]);
    }

    public function update(Request $request, $id)
    {
        $lotUpdate = Lot::where('id', $id)
        ->update([
            'name' => $request->input('name'),
            'description' => $request->input('description')
        ]);

        if($lotUpdate){
            return redirect()->route('lots.show', ['lot'=>$id]);
        }
        return redirect()->route('lots.show', ['lot'=>$id]);
    }

    public function destroy($id)
   {
       $find = Lot::find($id);
       if($find->delete())
       {
            return redirect()->route('lots.index');
       }
       return back();
   }

}
