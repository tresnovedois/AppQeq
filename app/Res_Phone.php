<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Researcher;

class Res_Phone extends Model
{
    protected $table = 'res_phones';
    protected $fillable = [
        'ddi',
        'ddd',
        'phone',
        'researcher_id'
    ];

    public $timestamps = false;

    public function researchers(){
        return $this->belongsTo('App\Researcher','researchers_id');
    }
}
