<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Researchers_Lots extends Model
{
    protected $table = 'researchers_lots';

    protected $fillable = [
        'researcher_id',
        'lot_id'
    ];

    public $timestamps = false;
}
