<!--LOTS EDIT-->

@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
   <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
      <div class="card-header">Editar dados de <a style="font-weight:bold">{{$lot->name}}</a></div>                  
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <ul class="list-group"></ul>  
            <li class="list-group-item">
                <form method="post" action="{{ route('lots.update', [$lot->id]) }}">

                {{ csrf_field() }}
            
                <input type="hidden" name="_method" value="put">
<div class="row">
                    <div class="form-group col-md-6">
                    <label for="lot-name">Nome do lote*</label>
                    <input type="text" 
                            class="form-control" 
                            id="lot-name" 
                            value="{{$lot->name}}"
                            name="name"
                            maxlength="45"
                            required
                            />
                    </div>

                    <div class="form-group col-md-6">
                    <label for="lot-id">Descrição</label>
                    <input type="text" 
                            class="form-control" 
                            id="lot-description" 
                            value="{{$lot->description}}"
                            name="description"
                            maxlength="45"
                            />
                    </div> 
                    </div> 
                        
                <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>
                <br>                                  
                <a class="btn btn-outline-danger" href="/lots/{{$lot->id}}">Cancelar</a>
                <input type="submit" class="btn btn-outline-success"  value="Salvar"/>
                    <!--FIM DO FORMULÁRIO-->
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

