<!--LOTS CREATE-->
@extends('layouts.app')

@section('content')


<div class="container" >
  <div class="row justify-content-center" style="margin-top: 5.5%">
    <div class="col-md-10">
        <div class="card">
          <div class="card-header"><a style="font-weight:bold">NOVO LOTE DE TRABALHO</a></div>                  
            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                    
                    <ul class="list-group"></ul>  
                    <li class="list-group-item">
                    <form method="post" action="{{ route('lots.store') }}">

                        {{ csrf_field() }}
                
                        
                        <div class="form-row">

                        <div class="form-group col-md-6">
                        <label for="lot-name">Nome do lote*</label>
                        <input type="text" 
                                class="form-control" 
                                id="lot-name" 
                                placeholder="Nome/n° do lote" 
                                name="name"
                                maxlength="45"
                                required
                                />
                        </div>

                        <div class="form-group col-md-6">
                        <label for="lot-id">Descrição</label>
                        <input type="text" 
                                class="form-control" 
                                id="lot-description" 
                                placeholder="Sobre este lote" 
                                name="description"
                                maxlength="45"
                                />
                        </div>  
                        
                        <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>
                        </div>                                     
                            
                        <br>
                        <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Salvar</button>
                    <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-times"></i> Cancelar</a>
                </form>                                                    
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


                   

