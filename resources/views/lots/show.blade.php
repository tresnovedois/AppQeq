<!--LOTS SHOW-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">
          
          Dados de <a style="font-weight:bold">{{$lot->name}}</a></div>                  
          <div class="card-body">
              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif  
              
            <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-info-circle"></i> Sobre este Lote de Trabalho</div>
            <div class="card" style="border-radius: 0%">
            <div style="margin-left:5px">
              Nome: {{$lot->name}} <br>
              Descrição: {{$lot->description}} <br>

              Membros: 
                @foreach($lot->researchers as $res)
                <a href="/researchers/{{$res->id}}">   
                {{$res->name}}
                </a>|
                @endforeach

            </div>
            </div>
<br>
              <div style="margin-left: 578px">
              <form method="POST">
              <a class="btn btn-primary" href="/lots"><i class="fas fa-bars"></i> Lotes de Trabalho</a>
              <a class="btn btn-warning" href="/lots/{{$lot['id']}}/edit">
              <i class="far fa-edit"></i> Editar</a>
              <input type="hidden" name="_method" value="DELETE" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}" >
              <input type="hidden" name="id" value = "{{$lot['id']}}">
              <input type="hidden" name="form_type" value = "deleteRes">
              <input
                  onclick="return confirm('Deletar TODOS os registros de {{$lot->name}}?');"
                  class="btn btn-danger"
                  value="Deletar"
                  style="cursor: pointer;"
                  type="submit"
              >
              </form>
              </div>
   </div>
  </div>                          
   </div>
    </div>
     </div>
</div>
</div>


@endsection

