<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Quem é Quem</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <style>
            /* googlapi */
            @font-face {
                font-family: 'Raleway';
                font-weight: 100;
                src: local('Raleway Thin'), local('Raleway-Thin'), url(https://fonts.gstatic.com/s/raleway/v12/1Ptsg8zYS_SKggPNwE44TYFq.woff2) format('woff2');
            }

            html, body {
                background: linear-gradient(to bottom, #7CB9E8, #42ffab);
                color: #ffff;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 100px;
                
            }

            .links > a {
                color: #ffff;
                padding: 0 5px;
                font-size: 19px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;              
            }

        </style>

    </head>
    
    <body>     
                     
        <div class="content">                 
            <div class="flex-center position-ref full-height">
                    <div class="title m-b-md">
                        <i class="fas fa-leaf"></i><br>Quem é Quem
                    <div class="links">
                            @if (Route::has('login'))                            
                                @auth
                                    <a href="{{ route('home') }}">Home</a>
                                @else
                                    <a href="{{ route('v.home') }}">Entrar</a>
                                    <a href="{{ route('register') }}">Criar Conta</a>
                                @endauth                            
                            @endif
                        </div>    
                    </div>                          
            </div>                    
        </div>

        
    </body>
</html>

{{-- Feito com café e dedicação por Arthur Neves Monteiro, aluno do 4° semestre de ADS do Senac 29/05/2018 --}}