<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Quem é Quem') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
                {{-- gmaps scripts --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://maps.google.com/maps/api/js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9mXtcu13rdl39cKzSezGnr5sTNx6dagM"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <style>
        .navbar-laravel {
            background: linear-gradient(to right, #7CB9E8, #42ffab);
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);
            width: 100%;
            z-index: 10;
            position:fixed;
        }

        .card-header {
            background: #7CB9E8;
            color: white;            
            text-transform: uppercase;
            box-shadow: 0px rgba(,0,0,0.61);
            font-size: larger;
        }

        .card-body-m {
            margin: 1%;
            height: 500px;            
        }
            
        .card-menu{
            height: 500px;
        }

        html, body {
         /* background-image: url("{{URL::asset('/image/leaf.jpg')}}"); */
         background: linear-gradient(to bottom, #7CB9E8, #f7f5f5);
         font-weight: 100;
         height: 100vh;
         margin: 0;
         font-family: 'Gill Sans', 'Gill Sans MT', 'Calibri', 'Trebuchet MS', sans-serif;
         font-size: medium;
         background-attachment: fixed;
        }   

        .bt-menu {
        width: 80%;
        margin-left: 25%;
        background: linear-gradient(to top, #7CB9E8, #42ffab);
        text-transform: uppercase;
        font-weight: bold;
        }

        .collapse-menu{
        text-transform: uppercase;
        text-align: center;
        font-weight: bold;

        }

        .greetings{
            font-family: 'Gill Sans', 'Gill Sans MT', 'Calibri', 'Trebuchet MS', sans-serif;
            font-size: 500%; 
            text-align: center;
            margin-top: 11%;
            color:white;
    
        }

        .p-img{
            border-radius: 50%;
            width: 35px;
            height: 35px;
        }

        .imgs{
            border-radius: 50%;
            width: 5%;
            height: 100%;
        }

        #map {
            border:1px solid;
            width: 800px;
            height: 500px;
    	}

        .boxes{
            margin-left:1%;
            margin-right:1%
        }

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel navbar-fixed-top" style="color: white">
            @guest
            @else
            <a class="navbar-brand"  style="color: white; font-weight:bold;" href="{{ url('/home') }}">    
                <i class="fas fa-leaf"></i> {{ config('','INÍCIO') }}
            </a>
            @endguest
            <div class="container">

                @guest
                <a style="color: white; font-weight:bold">QUEM É QUEM</a>
                @else
                <a class="navbar-brand"  style="color: white; font-weight:bold; font-size:150%" href="{{ URL::previous() }}">    
                  <i class="fas fa-arrow-alt-circle-left"></i>
                </a>

                <div class="dropdown" style="margin-left:44.6%">
                    <button class="btn btn-outline-light dropdown-toggle" type="button" id="inst" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-university"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="inst">
                        <h6 class="dropdown-header">INSTITUIÇÕES</h6>
                        <a class="dropdown-item" href="{{ url('/establishments') }}"><i class="fas fa-bars" style="color:blue"></i> Listar Instituições</a>
                        <a class="dropdown-item" href="{{ url('establishments/create') }}"><i class="fas fa-plus" style="color:blue"></i> Adicionar Instituição</a>
                    </div>
                    
                </div>
            
                <div class="dropdown" style="margin-left:5%">
                    <button class="btn btn-outline-light dropdown-toggle" type="button" id="res" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-users"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="res">
                            <h6 class="dropdown-header">PESQUISADORES</h6>
                            <a class="dropdown-item" href="{{ url('/researchers') }}"><i class="fas fa-bars" style="color:blue"></i> Listar Pesquisadores</a>
                            <a class="dropdown-item" href="{{ url('/researchers/create') }}"><i class="fas fa-plus" style="color:blue"></i> Adicionar Pesquisadores</a>
                    </div>
                </div>   

                <div class="dropdown" style="margin-left:5%">
                    <button class="btn btn-outline-light dropdown-toggle" type="button" id="res" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-briefcase"></i>
                    </button>

                    <div class="dropdown-menu" aria-labelledby="res">
                            <h6 class="dropdown-header">LOTES DE TRABALHO</h6>
                            <a class="dropdown-item" href="{{ url('/lots') }}"><i class="fas fa-bars" style="color:blue"></i> Listar Lotes de Trabalho</a>
                            <a class="dropdown-item" href="{{ url('/lots/create') }}"><i class="fas fa-plus" style="color:blue"></i> Adicionar Lotes de Trabalho</a>
                    </div>
                </div>                   
                    
                @endguest

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <!-- Right Side Of Navbar -->                    
                </div>
            </div>   
            <ul class="navbar-nav ml-auto" style="margin-right:4%">
            <!-- Authentication Links -->
            @guest
                <li><a class="nav-link" href="{{ route('login') }}"style="color: white">{{ __('ENTRAR') }}</a></li>
                <li><a class="nav-link" href="{{ route('register') }}"style="color: white">{{ __('REGISTRAR-SE') }}</a></li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: white">
                        <i class="fas fa-user-circle"></i> {{ Auth::user()->name }}<span class="caret"></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <div style="color: red">
                            {{ __('Sair') }} <i class="fas fa-sign-out-alt"></i>
                        </div>
                        <a class="dropdown-item" href="{{ route('v.home') }}">
                                <div style="color: grey">
                                    <i>Alternar para Visitante</i> <i class="fas fa-user-clock"></i>
                                </div>
                            </a>

                        <a class="dropdown-item" href="{{ route('welcome') }}">
                            <div style="color: blue">
                              Tela inicial <i class="fas fa-leaf"></i> 
                            </div>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
            </ul>
        </nav>
        
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>