<!--RESEARCHERS CREATE-->
@extends('layouts.app')

@section('content')


<div class="container" >
    <div class="row justify-content-center" style="margin-top: 5.5%">
        <div class="col-md-10">
            <div class="card">
              <div class="card-header"><a style="font-weight:bold">NOVO PESQUISADOR</a></div>                  
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                     
                        <ul class="list-group"></ul>  
                        <li class="list-group-item">
                        <form method="post" action="{{ route('researchers.store') }}">

                          {{ csrf_field() }}
                    
                          
                          <div class="form-row">

                          <div class="form-group col-md-10">
                          <label for="researcher-name">Nome Completo*</label>
                          <input type="text" 
                                 class="form-control" 
                                 id="researcher-name" 
                                 placeholder="Nome Completo do Pesquisador" 
                                 name="name"
                                 maxlength="60"
                                 required
                                 />
                          </div>

                          <div class="form-group col-md-2">
                            <label for="researcher-id">CPF*</label>
                            <input type="text" 
                                   class="form-control" 
                                   id="researcher-id" 
                                   placeholder="Apenas números!" 
                                   name="national_id"
                                   maxlength="45"
                                   required
                                   />
                            </div>  

                          </div>

                          <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="email">Email*</label>
                            <input type="email" 
                                    class="form-control" 
                                    id="inputPassword4" 
                                    placeholder="exemplo@email.com"
                                    name ="email"
                                    maxlength="45"
                                    required/>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="researcher-ddi">DDI*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="researcher-ddi" 
                                    placeholder="Ex:55" 
                                    name="ddi"
                                    maxlength="4"
                                    required
                                    />
                        </div>  

                        <div class="form-group col-md-1">
                            <label for="researcher-ddd">DDD*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="researcher-ddd" 
                                    placeholder="Ex:61" 
                                    name="ddd"
                                    maxlength="2"
                                    required
                                    />
                        </div>  

                        <div class="form-group col-md-2">
                            <label for="researcher-phone">Telefone*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="researcher-phone" 
                                    placeholder="Apenas números!" 
                                    name="phone"
                                    maxlength="10"
                                    required
                                    />
                        </div>

                        <div class="form-group col-md-4">
                            <label for="researcher-link">Link do <a href="http://buscatextual.cnpq.br/buscatextual/busca.do?metodo=apresentar">Currículo Lattes</a></label>
                            <input type="text" 
                                    class="form-control" 
                                    id="researcher-link" 
                                    placeholder="Ex: http://cnpq.br/visualizacv.do?id=xxxx " 
                                    name="url_curriculum"
                                    maxlength="100"
                                    
                                    />
                        </div>                    
                        </div>
                         
                        Resumo do Currículo: <br>
                          <div class="input-group">      
                              <textarea class="form-control" 
                                        aria-label="With textarea"
                                        name="c_summary"
                              ></textarea>
                            </div>
                            <br>

                            <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="researcher-zip">CEP*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="researcher-zip" 
                                        placeholder="Apenas números!" 
                                        name="postal_code"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-4">
                                <label for="researcher-address">Endereço*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="researcher-address" 
                                        placeholder="Ex: Rua das Artes, Bairro Ipê casa 32" 
                                        name="address"
                                        maxlength="90"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-2">
                                <label for="researcher-city">Cidade*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="researcher-city" 
                                        placeholder="Ex:Brasília" 
                                        name="city"
                                        maxlength="50"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-1">
                                    <label for="researcher-state">UF*</label>
                                    <input type="text" 
                                    class="form-control" 
                                    id="researcher-state" 
                                    placeholder="Ex: DF" 
                                    name="state"
                                    maxlength="2"
                                    required
                                    />
                                    </div>   

                                <div class="form-group col-md-3">
                                    <label for="researcher-country">País*</label>
                                    <input type="text" 
                                    class="form-control" 
                                    id="researcher-country" 
                                    placeholder="Brasil" 
                                    name="country"
                                    maxlength="45"
                                    required
                                    />
                                </div>
                           <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>
                            </div>                                     
                                
                            <br>
                          <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Salvar</button>
                        <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-times"></i> Cancelar</a>
                  </form>                                                    
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


                   

