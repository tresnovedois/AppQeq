<!--RESEARCHERS INDEX-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">                
              <div class="card-header"><i class="fas fa-users"></i> Banco de Pesquisadores </div> 
                <div class="card-body">
                        
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="btn btn-primary" href="/researchers/create"><i class="fas fa-plus"></i> Novo Pesquisador</a> <br>

                    @foreach($researchers as $researcher)
                    <br>
                    <li class="list-group-item">
                        @if ($researcher->url_photo)
                        <a href="/researchers/{{ $researcher->id }}">
                          <img src="{{ URL($researcher->url_photo) }}" class="p-img">
                        </a>
                        @else  
                        <a href="/researchers/{{ $researcher->id }}">
                          <img src="{{ URL('image/empty.png') }}" class="p-img">
                        </a>
                        @endif 
                    <a href="/researchers/{{ $researcher->id }}">  
                    {{ $researcher->name }}
                    </a>                           
                    @endforeach 
                    </li>                   
                    <br>
             
</div>     
</div>
</div>
</div>
</div>
@endsection