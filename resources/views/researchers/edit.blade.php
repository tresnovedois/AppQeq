<!--RESEARCHERS EDIT-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
   <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
      <div class="card-header">Editar dados de <a style="font-weight:bold">{{$researcher->name}}</a></div>                  
          <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <ul class="list-group"></ul>  
            <li class="list-group-item">
              <form method="post" action="{{ route('researchers.update', [$researcher->id]) }}">

                {{ csrf_field() }}
          
                <input type="hidden" name="_method" value="put">

                  <div class="form-row">
                    <div class="form-group col-md-7">
                      <label for="researcher-name">Nome*</label>
                      <input required 
                              type="text" 
                              class="form-control" 
                              id="researcher-name" 
                              value="{{ $researcher->name}}"
                              maxlength="60"
                              name="name"                                   
                              />
                      </div>
                    <div class="form-group col-md-5">
                      <label for="researcher-email">Email*</label>
                      <input required
                              type="email" 
                              class="form-control" 
                              id="researcher-email" 
                              value="{{ $researcher->email}}"
                              maxlength="45"
                              name="email"
                              />
                    </div>
                  </div>
                    
                    <div class="form-row">

                      <div class="form-group col-md-4">
                      <label for="researcher-id">CPF*</label>
                      <input type="text" 
                              class="form-control" 
                              id="researcher-id" 
                              value="{{ $researcher->national_id}}" 
                              name="national_id"
                              maxlength="45"
                              required
                              />
                      </div> 
                      
                      <div class="form-group col-md-8">
                        <label for="researcher-link">Link do <a href="http://buscatextual.cnpq.br/buscatextual/busca.do?metodo=apresentar">Currículo Lattes</a>*</label>
                        <input type="text" 
                                class="form-control" 
                                id="researcher-link" 
                                value="{{ $researcher->url_curriculum}}" 
                                name="url_curriculum"
                                maxlength="100"
                                required
                                />
                        </div>  

                      </div>  

                      <div class="form-row">

                        Resumo do Currículo* <br>
                      <div class="input-group">      
                          <textarea class="form-control" 
                                    aria-label="With textarea"
                                    name="c_summary"
                                    required
                          >{{ $researcher->c_summary}}
                        </textarea>
                        </div>
                        <br>
            <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos!(*)</i></a>
            <div style="margin-left:80%">
            <a class="btn btn-outline-danger" href="/researchers/{{$researcher->id}}">Cancelar</a>
            <input type="submit" class="btn btn-outline-success"  value="Salvar"/>

</div>
                  <!--FIM DO FORMULÁRIO-->
                  </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

