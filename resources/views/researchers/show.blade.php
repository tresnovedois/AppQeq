<!--RESEARCHERS SHOW-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">
            @if ($researcher->url_photo)
                <img src="{{ URL($researcher->url_photo) }}" class="p-img">
            @else  
            <img src="{{ URL('image/empty.png') }}" class="p-img">
            @endif
          
          Dados de <a style="font-weight:bold">{{$researcher->name}}</a></div>                  
          <div class="card-body">
              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif  
                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-user"></i> INFORMAÇÕES PESSOAIS</div>
                <div class="card" style="border-radius: 0%">
                <div class="row">
                  <div class="group col-md-4">
                  <div class="boxes">
                  NOME: {{$researcher->name}}<br>
                  EMAIL: {{$researcher->email}}<br>
                  CPF: {{$researcher->national_id}}<br>
                  </div>                         
                </div>
                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#1">
                  <i class="fas fa-graduation-cap"></i> Currículo
                </button>
                <div style="margin-left:40%; margin-top:2%"><a class="btn btn-warning" href="/researchers/{{$researcher['id']}}/edit"><i class="far fa-edit"></i> Editar</a></div>
                </div> 
                </div>

                <br>

                {{-- CADASTRO DE FOTO --}}

                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-camera-retro"></i> Foto</div>
                <div class="card" style="border-radius: 0%">
                                
                  <div class="row">
                  <div class="col-md-2" style="margin:10px">
                
                    @if ($researcher->url_photo)
                        
                      <img src="{{ URL($researcher->url_photo) }}" style="width:150px;  height: auto">
  
                    @else  
                      <img src="{{ URL('image/empty.png') }}" class="p-img" style="width:150px;  height: auto">
                    @endif
                        
                    </div>
                
                    <div class="col-md-4">
                      {{Form::open(array('route' => 'imagePostResearcher','enctype' => 'multipart/form-data'))}}
                      {!! Form::file('image', array('class' => 'image', 'style' => 'display:none', 'id' => 'file')) !!}
                      {{-- envia o id para a função --}}
                      <input type="text" 
                        class="form-control"
                        value="{{$researcher->id}}" 
                        name="id"
                        style="display:none"
                        required
                        />
                      {{-- envia o id para a função --}}
                      <input class="btn btn-link" type="button" id="image" value="Buscar imagem" onclick="document.getElementById('file').click();"/>
                      <i class="fas fa-search" style="color:blue"></i><br>

                      <button type="submit" class="btn btn-link" style="color:green">
                        Enviar 
                      </button><i class="fas fa-share-square" style="color:green"></i>
                      {!! Form::close() !!}
                    
                    <form method="post" action="{{ route('r.deletephoto' , [$researcher->id]) }}">

                    {{ csrf_field() }}
              
                    <input type="hidden" name="_method" value="put">

                    <input type="text" 
                           class="form-control"
                           value="{{$researcher->id}}" 
                           name="id"
                           style="display:none"
                           required
                    />

                    <input  type="text" 
                            style="display:none"
                            value=""
                            id="researcher-url-photo"
                            name="name"                                   
                            />

                    <input  class="btn btn-link"
                            value="Excluir"
                            onclick= "return confirm('Tem certeza de que deseja excluir a foto atual?')"
                            style="margin-left:1px; color:red"                            
                            type="submit"
                            /><i class="fas fa-times" style="color:red"></i>
                    <br>
                    </form>                  
                  </div>
                
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block col-md-4" style="height:50px; margin-top: 2px">
                          <button type="button" class="close" data-dismiss="alert">x</button>	
                            Imagem atualizada!
                        </div>
                      @endif

                </div>
              </div>
                {{-- FIM CADASTRO DE FOTO --}}
                <br>
   
                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-briefcase"></i> LOTE DE TRABALHO 
                </div>
                <div class="card" style="border-radius: 0%">

                <div class="row">
                <div class="col-md-10" style="margin-left: 1%">

                                        
                  @foreach($researcher->lots as $lot) 
                <div class="boxes" style="margin-top:1.5%">                 
                  <a href="/lots/{{$lot->id}}">
                    {{$lot->name}}<br>
                  </a> 
                </div>
                @endforeach    
                </div>

                <div class="col-md-1">
                <div class="boxes"></div>    
                  @foreach($rl as $rl)
                  <form method="post" action="{{ route('deleteLot') }}">                  
                  <a href="/lots/{{$rl->id}}"></a>
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value = "{{$rl->id}}">  
                    <div class="boxes" style="margin-top:7%">                     
                    <button onclick="return confirm('Tem certeza de que deseja desvincular {{$researcher->name}} deste lote de trabalho?')" 
                    type="submit" class="btn btn-link" style="color:red">
                    <i class="fas fa-times"></i>
                    </button></div>         
                  </form>
                  @endforeach
                  </div> 
                  </div>
   
              </div>      
              <br>
              
              <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-phone"></i> TELEFONES 
                </div>
                <div class="card" style="border-radius: 0%">
                  <div class="boxes">                          
                    @foreach($researcher->resPhones as $res_phone)
                    <form method="post" action="{{ route('r.deletep') }}">
                    +{{$res_phone->ddi}}
                    ({{$res_phone->ddd}})
                    {{$res_phone->phone}}                          
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value = "{{$res_phone->id}}">
                    <button type="submit" class="btn btn-link" style="font-size:70%; color:red" 
                    onclick="return confirm('Tem certeza de que deseja excluir o número: +{{$res_phone->ddi}} ({{$res_phone->ddd}}) {{$res_phone->phone}}?')">
                    <i class="fas fa-times"></i>
                    </button>                                 
                    </form>          
                    @endforeach    
                </div>
              </div>

              <br>

            <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-location-arrow"></i> ENDEREÇOS 
            </div>
            <div class="card" style="border-radius: 0%">
              <div class="boxes" style="margin-top:1%">
                @foreach($researcher->adrResearchers as $adr)                           
                <form method="post" action="{{ route('r.deletea') }}"> 
                Endereço: {{$adr->address}}<br>
                CEP: {{$adr->postal_code}}<br>
                Cidade: {{$adr->city}}-{{$adr->state}}<br>
                {{$adr->country}}<br>
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value = "{{$adr->id}}">        
                <button type="submit" class="btn btn-link" style="color:red" 
                onclick="return confirm('Tem certeza de que deseja excluir {{$adr->address}}?')">
                <i class="fas fa-trash-alt"></i> Excluir
                </button>
                <a href="{{route('r.showeditaddress', $adr->id)}}" class="btn btn-link">
                    <i class="far fa-edit"></i> Editar
                </a> 
                <br> ---------------------------------------------------------------------------------------------------------- <br>  
                </form> 
                @endforeach                                                                                           
                </div>             
            </div>
          <br>

          <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-university"></i> INSTITUIÇÕES 
          </div>
          
          <div class="card" style="border-radius: 0%">
              <div class="row">

              <div class="col-md-10" style="margin-left: 1%">
                
                @foreach($researcher->establishments as $est) 
                <div class="boxes" style="margin-top:1.5%">                 
                  <a href="/establishments/{{$est->id}}">
                    {{$est->name}}<br>
                  </a> 
                </div>
                @endforeach    
              
              </div>            

          <div class="col-md-1"> 
          @foreach($er as $er)
          <form method="post" action="{{ route('unlink') }}">                  
          <a href="/establishments/{{$er->id}}"></a>
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value = "{{$er->id}}">  
            <div class="boxes" style="margin-top:7%">                     
            <button onclick="return confirm('Tem certeza de que deseja desvincular {{$researcher->name}} desta instituição?')" 
            type="submit" class="btn btn-link" style="color:red">
            <i class="fas fa-times"></i>
            </button></div>         
          </form>
          @endforeach 
          </div>

          </div>
          </div>
<br>

<!-- Currículo-->
      
  <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="1">Resumo do currículo de {{$researcher->name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{$researcher->c_summary}}<br>
          <br>
        <i class="fas fa-link"></i> Link Currículo Lattes: <a href="#">{{$researcher->url_curriculum}}</a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
    
<!--VINCULAR INSTITUIÇÃO-->

<div class="modal fade" id="2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="2">Instituições de {{$researcher->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">                

          <form method="post" action="{{ route('r.link') }}">
          <input type="hidden" name="_method" value="PUT" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}" >
          <input type="hidden" name="id" value = "{{$researcher['id']}}">
          
          <select class="custom-select col-md-6" id="inputGroupSelect01" name="establishmentId" required>
          <option>Escolher Instituição</option>
            @foreach($establishments as $establishment)                
              <option  value="{{$establishment->id}}">
              {{$establishment['name']}}
              </option> 
              @endforeach               
          </select>                
            <br><br>
            <input
              class="btn btn-success"
              value="Vincular"
              style="cursor: pointer;"
              type="submit"
          >
          </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!--VINCULAR LOTE DE TRABALHO-->

<div class="modal fade" id="5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="2">Lotes de Trabalho de {{$researcher->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">                

          <form method="post" action="{{ route('r.lot') }}">
          <input type="hidden" name="_method" value="PUT" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}" >
          <input type="hidden" name="id" value = "{{$researcher['id']}}">
          
          <select class="custom-select col-md-6" id="inputGroupSelect01" name="lotId" required>
          <option>Escolher Lote</option>
            @foreach($lots as $lot)                
              <option  value="{{$lot->id}}">
              {{$lot['name']}}
              </option> 
              @endforeach               
          </select>                
            <br><br>
            <input
              class="btn btn-success"
              value="Vincular"
              style="cursor: pointer;"
              type="submit"
          >
          </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!--Novos TELEFONES-->

<div class="modal fade" id="4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="1">Novo endereço de {{$researcher->name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{ route('r.newphone') }}">
              <input type="hidden" name="_method" value="PUT" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}" >
              <input type="hidden" name="id" value = "{{$researcher['id']}}">

              <div class="form-row">
              <div class="form-group col-md-2">
                <label for="researcher-ddi">DDI*</label>
                <input type="text" 
                        class="form-control" 
                        id="researcher-ddi" 
                        placeholder="Ex: 55" 
                        name="ddi"
                        maxlength="4"
                        required
                        />
              </div>  

              <div class="form-group col-md-2">
                <label for="researcher-ddd">DDD*</label>
                <input type="text" 
                        class="form-control" 
                        id="researcher-ddd" 
                        placeholder="Ex: 61" 
                        name="ddd"
                        maxlength="2"
                        required
                        />
              </div>  

              <div class="form-group col-md-3">
                <label for="researcher-phone">Telefone*</label>
                <input type="text" 
                        class="form-control" 
                        id="researcher-phone" 
                        placeholder="Apenas números!" 
                        name="phone"
                        maxlength="10"
                        required
                        />
              </div>
              </div>
              <button type="submit" class="btn btn-success"> Adicionar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

<!--Novos Endereços-->

<div class="modal fade" id="3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="1">Novo endereço de {{$researcher->name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="{{ route('r.newad') }}">
              <input type="hidden" name="_method" value="PUT" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}" >
              <input type="hidden" name="id" value = "{{$researcher['id']}}">

              <div class="form-row">
                  <div class="form-group col-md-2">
                      <label for="researcher-zip">CEP*</label>
                      <input type="text" 
                              class="form-control" 
                              id="researcher-zip" 
                              placeholder="Apenas números!" 
                              name="postal_code"
                              maxlength="20"
                              required
                              />
                  </div>

                  <div class="form-group col-md-4">
                      <label for="researcher-address">Endereço*</label>
                      <input type="text" 
                              class="form-control" 
                              id="researcher-address" 
                              placeholder="Ex: Rua das Artes, Bairro Ipê casa 32" 
                              name="address"
                              maxlength="90"
                              required
                              />
                  </div>

                  <div class="form-group col-md-2">
                      <label for="researcher-city">Cidade*</label>
                      <input type="text" 
                              class="form-control" 
                              id="researcher-city" 
                              placeholder="Ex:Brasília" 
                              name="city"
                              maxlength="50"
                              required
                              />
                  </div>

                  <div class="form-group col-md-1">
                          <label for="researcher-state">UF*</label>
                          <input type="text" 
                          class="form-control" 
                          id="researcher-state" 
                          placeholder="Ex: DF" 
                          name="state"
                          maxlength="2"
                          required
                          />
                          </div>   

                      <div class="form-group col-md-3">
                          <label for="researcher-country">País*</label>
                          <input type="text" 
                          class="form-control" 
                          id="researcher-country" 
                          placeholder="Brasil" 
                          name="country"
                          maxlength="90"
                          required
                          />
                      </div>
              </div>
              <button type="submit" class="btn btn-success"> Adicionar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
        
<!--botões-->
<div> 
   
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#2">
      <i class="fas fa-university"></i> Vincular Instituição
    </button>   

     <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#5">
      <i class="fas fa-briefcase"></i> Vincular Lote de Trabalho
    </button>                                      

    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#3">
      <i class="fas fa-location-arrow"></i> Novo Endereço
    </button>
    
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#4">
      <i class="fas fa-phone"></i> Novo Telefone
    </button>
    
</div>

{{-- DELETAR/MOSTRAR PESQUISADOR --}}
<br>
<div style="margin-left:78%">
  
  <form method="POST">
    <a class="btn btn-primary" href="/researchers"><i class="fas fa-bars"></i> Pesquisadores</a>
    <input type="hidden" name="_method" value="DELETE" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
    <input type="hidden" name="id" value = "{{$researcher['id']}}">
    <input type="hidden" name="form_type" value = "deleteRes">  
    <input
        onclick="return confirm('Deletar TODOS os registros de {{$researcher->name}}?');"
        class="btn btn-danger"
        value="Excluir"
        style="cursor: pointer;"
        type="submit"
    >
  </form>
</div>

   </div>
  </div>                          
   </div>
    </div>
     </div>
</div>
</div>


@endsection

