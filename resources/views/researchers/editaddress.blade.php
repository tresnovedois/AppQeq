{{-- RESEARCHER ADDRESS --}}
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
  <div class="row justify-content-center">
   <div class="col-md-10">
    <div class="card">
     <div class="card-header">Editar Endereço</a></div>                  
      <div class="card-body">
        @if (session('status'))
         <div class="alert alert-success">
          {{ session('status') }}
         </div>
         @endif 

         <form method="post" action="{{ route('r.editaddress', [$adr->id]) }}">

            {{ csrf_field() }}
      
        <input type="hidden" name="_method" value="put">
        
          <div class="form-row">
            <div class="form-group col-md-9">
              <label for="researcher-address">Endereço*</label>
              <input type="text" 
                     class="form-control" 
                     id="researcher-address" 
                     value="{{$adr->address}}" 
                     name="address"
                     maxlength="90"
                     required
                    />
            </div>
            
            <div class="form-group col-md-3">
              <label for="researcher-zip">CEP*</label>
              <input type="text" 
                     class="form-control" 
                     id="researcher-zip" 
                     value="{{$adr->postal_code}}" 
                     name="postal_code"
                     maxlength="20"
                     required
                     />
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-5">
              <label for="researcher-city">Cidade*</label>
              <input type="text" 
                     class="form-control" 
                     id="researcher-city" 
                     value="{{$adr->city}}" 
                     name="city"
                     maxlength="50"
                     required
                     />
            </div>

            <div class="form-group col-md-1">
             <label for="researcher-state">UF*</label>
             <input type="text" 
                   class="form-control" 
                   id="researcher-state" 
                   value="{{$adr->state}}" 
                   name="state"
                   maxlength="2"
                   required
                   />
                </div>   
              

              <div class="form-group col-md-6">
                <label for="researcher-country">País*</label>
                <input type="text" 
                       class="form-control" 
                       id="researcher-country" 
                       value="{{$adr->country}}" 
                       name="country"
                       maxlength="45"
                       required
                       />
              </div>

              <input type="text" 
                     class="form-control"
                     value="{{$adr->researcher_id}}" 
                     name="researcher_id"
                     style="display:none"
                     required
                     />

            </div>

<a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>

        <div style="margin-left:80%">
            <a class="btn btn-outline-danger" href="{{ URL::previous() }}">Cancelar</a>
            <input type="submit" class="btn btn-outline-success"  value="Salvar"/>
        </div>

        </form> 
</div>
</div>
</div>
</div>
</div>

@endsection