@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<?php
use Carbon\Carbon;
//phpinfo(); 
$dt = Carbon::today();
?>

<div class="greetings col-md-12">
Olá, {{ Auth::user()->name }}!<i class="fab fa-envira"></i>
</div>

<div class="col-md-12" style="position:fixed; margin-left:23%">
<div class="row">
    
    <div class="col-md-2">
    <div class="dropdown">
    <a class="btn btn-secondary dropdown-toggle bt-menu"  href="#establishment" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-university"></i> INSTITUIÇÕES
    </a>
    
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    
    <a class="dropdown-item" style="color: blue" href="{{ url('/establishments') }}">        
        <i class="fas fa-bars"></i> MOSTRAR 
    </a>
    <a class="dropdown-item" style="color: blue" href="{{ url('/establishments/create') }}">
        <i class="fas fa-plus"></i> ADICIONAR 
    </a>
    </div>
    </div>
    </div>
    
    <div class="col-md-2">
    <div class="dropdown">
    <a class="btn btn-secondary dropdown-toggle bt-menu" style="margin-left:20%" href="#establishment" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-users"></i> PESQUISADORES
    </a>
    
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" style="color: blue" href="{{ url('/researchers') }}">        
        <i class="fas fa-bars"></i> MOSTRAR 
    </a>
    <a class="dropdown-item" style="color: blue" href="{{ url('/researchers/create') }}">
        <i class="fas fa-plus"></i> ADICIONAR 
    </a>
    </div>
    </div>
    </div>

    <div class="col-md-2">
    <div class="dropdown">
    <a class="btn btn-secondary dropdown-toggle bt-menu" style="margin-left:15%"  href="#establishment" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-briefcase"></i> LOTES DE TRABALHO
    </a>
    
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    
    <a class="dropdown-item" style="color: blue" href="{{ url('/lots') }}">        
        <i class="fas fa-bars"></i> MOSTRAR 
    </a>
    <a class="dropdown-item" style="color: blue" href="{{ url('/lots/create') }}">
        <i class="fas fa-plus"></i> ADICIONAR 
    </a>
    </div>
    </div>
    </div>

   </div>
</div>
@endsection

