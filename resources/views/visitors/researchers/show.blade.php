<!--VISITORS RESEARCHERS SHOW-->
@extends('visitors.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">
            @if ($researcher->url_photo)
                <img src="{{ URL($researcher->url_photo) }}" class="p-img">
            @else  
            <img src="{{ URL('image/empty.png') }}" class="p-img">
            @endif
          
          Dados de <a style="font-weight:bold">{{$researcher->name}}</a></div>                  
          <div class="card-body">
                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-user"></i> INFORMAÇÕES PESSOAIS</div>
                <div class="card" style="border-radius: 0%">
                <div class="row">
                  <div class="group col-md-4">
                  <div class="boxes">
                  NOME: {{$researcher->name}}<br>
                  EMAIL: {{$researcher->email}}<br>
                  CPF: {{$researcher->national_id}}<br>

                  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#1">
                    <i class="fas fa-graduation-cap"></i> Currículo
                  </button>

                  <br>
                  <div>
                  @if ($researcher->url_photo)  
                    <img src="{{ URL($researcher->url_photo) }}" style="width:150px;  height: auto">
                  @else  
                    <img src="{{ URL('image/empty.png') }}" class="p-img" style="width:150px;  height: auto">
                  @endif
                  </div>
                  <br>
                  </div>                         
                </div>
                </div> 
                </div>             

                <br>

                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-phone"></i> TELEFONES 
                </div>
                <div class="card" style="border-radius: 0%">
                  <div class="boxes">                          
                    @foreach($researcher->resPhones as $res_phone)
                    
                    +{{$res_phone->ddi}}
                    ({{$res_phone->ddd}})
                    {{$res_phone->phone}}                          
               
                         
                    @endforeach    
                </div>
              </div>
<br>
<div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-phone"></i> LOTES DE TRABALHO 
                </div>
                <div class="card" style="border-radius: 0%">
                  <div class="boxes">                          
                    @foreach($researcher->lots as $lot)                     
                      {{$lot->name}} <br>
                    @endforeach    
                </div>
              </div>
<br>
            <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-location-arrow"></i> ENDEREÇOS 
            </div>
            <div class="card" style="border-radius: 0%">
              <div class="boxes" style="margin-top:1%">
                @foreach($researcher->adrResearchers as $adr)                           
                
                Endereço: {{$adr->address}}<br>
                CEP: {{$adr->postal_code}}<br>
                Cidade: {{$adr->city}}-{{$adr->state}}<br>
                {{$adr->country}}<br>
      
                
                <br> ---------------------------------------------------------------------------------------------------------- <br>  
                </form> 
                @endforeach                                                                                           
                </div>             
            </div>
          <br>

          <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-university"></i> INSTITUIÇÕES 
          </div>
          
          <div class="card" style="border-radius: 0%">
              <div class="row">

              <div class="col-md-10" style="margin-left: 1%">
                
                @foreach($researcher->establishments as $est) 
                <div class="boxes" style="margin-top:1.5%">                 
                  <a href="{{ URL::route('e.show',$est->id) }}">
                    {{$est->name}}<br>
                  </a> 
                </div>
                @endforeach    
              </div>          

          </div>
          </div>
<br>

<!-- Currículo-->
      
      <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="1">Resumo do currículo de {{$researcher->name}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              {{$researcher->c_summary}}<br>
              <br>
            <i class="fas fa-link"></i> Link Currículo Lattes: <a href="#">{{$researcher->url_curriculum}}</a>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
          </div>
        </div>
      </div>
<br>
    <a class="btn btn-primary" href="{{URL::route('r.index')}}"><i class="fas fa-bars"></i> Pesquisadores</a>  

   </div>
  </div>                          
   </div>
    </div>
     </div>
</div>
</div>


@endsection

