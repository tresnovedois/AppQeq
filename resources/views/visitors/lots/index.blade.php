<!--LOTS INDEX-->
@extends('visitors.app')

@section('content')
<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">               
        <div class="card-header"><i class="fas fa-university"></i> Lotes de Trabalho</div>
          <div class="card-body">

            @foreach($lots as $lot)
            <br>
            <li class="list-group-item">

            <a href="/lots/{{ $lot->id }}">  
            {{ $lot->name }}
            </a>                           
            @endforeach 
            </li>                   
            <br>
             
</div>     
</div>
</div>
</div>
</div>

@endsection