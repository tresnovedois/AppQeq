@extends('visitors.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


<div class="greetings col-md-12">
Bem-Vindo! <i class="fas fa-user-clock"></i>
</div>

<div class="col-md-12" style="position:fixed; margin-left:23%">
<div class="row">
    
    <div class="col-md-2">
    <div class="dropdown">

    <a class="btn btn-secondary bt-menu"  href="{{ url('home2/establishments') }}">
            <i class="fas fa-university"></i> INSTITUIÇÕES
    </a>
    
    </div>
    </div>
    
    <div class="col-md-2">
    <div class="dropdown">
    <a class="btn btn-secondary bt-menu" style="margin-left:20%" href="{{ url('home2/researchers') }}">
            <i class="fas fa-users"></i> PESQUISADORES
    </a>
    
    </div>
    </div>

     <div class="col-md-2">
     <div class="dropdown">
     <a class="btn btn-secondary bt-menu" style="margin-left:15%"  href="{{ url('home2/lots') }}">
        <i class="fas fa-briefcase"></i> LOTES DE TRABALHO
     </a>
     </div>
     </div>

     </div>
</div>
@endsection

