<!--ESTABLISHMENTS INDEX-->
@extends('visitors.app')

@section('content')
<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">               
        <div class="card-header"><i class="fas fa-university"></i> Banco de Instituições</div>
          <div class="card-body">

            @foreach($establishments as $establishment)
            <br>
            <li class="list-group-item">
            @if ($establishment->url_logo)
            <a href="{{ URL::route('e.show',$establishment->id) }}">
                <img src="{{ URL($establishment->url_logo) }}" class="p-img">
            </a>
            @else  
            <a href="{{ URL::route('e.show',$establishment->id) }}">
              <img src="{{ URL('image/empty.png') }}" class="p-img">
            </a>
            @endif 
            <a href="{{ URL::route('e.show',$establishment->id) }}">  
            {{ $establishment->name }}
            </a>                           
            @endforeach 
            </li>                   
            <br>
             
</div>     
</div>
</div>
</div>
</div>

@endsection