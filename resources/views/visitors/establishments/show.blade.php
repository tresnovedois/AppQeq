<!--VISITORS ESTABLISHMENTS SHOW-->
@extends('visitors.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                  <div class="card-header">
                    @if ($establishment->url_logo)
                          <img src="{{ URL($establishment->url_logo) }}" class="p-img">
                      @else  
                          <img src="{{ URL('image/empty.png') }}" class="p-img">
                      @endif 
                    Dados de <a style="font-weight:bold">{{$establishment->name}}</a></div>                  
                    <div class="card-body">
    
                        <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-building"></i> DADOS DA EMPRESA</div>
                        <div class="card" style="border-radius: 0%">
                        <div class="row">
                            <div class="group col-md-4">
                            <div class="boxes">
                            NOME: {{$establishment->name}}<br>
                            EMAIL: {{$establishment->email}}<br>
                            CNPJ: {{$establishment->national_id}}<br>
                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#1">
                              <i class="fas fa-info"></i> Descrição
                            </button>
                            <br>
                            <div>
                            @if ($establishment->url_logo)
                                
                            <img src="{{ URL($establishment->url_logo) }}" style="width:150px;  height: auto">
          
                            @else  
                              <img src="{{ URL('image/empty.png') }}" class="p-img" style="width:150px;  height: auto">
                            @endif
                          </div>
                          <br>
                            </div>                         
                        </div>
                      </div> 
                      </div>
                      <br>
  
                      <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-phone"></i> TELEFONES</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                          @foreach($establishment->estPhones as $est_phone)
                          
                          +{{$est_phone->ddi}}
                          ({{$est_phone->ddd}})
                          {{$est_phone->phone}}
                          
                          @endforeach    
                      </div>
                    </div>
                      <br>
                    <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-location-arrow"></i> ENDEREÇOS</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                          @foreach($establishment->adrEstablishments as $adr)
                         
                          Endereço: {{$adr->address}}<br>
                          CEP: {{$adr->postal_code}}<br>
                          Cidade: {{$adr->city}}-{{$adr->state}}<br>
                          {{$adr->country}}<br>
                          Latitude: {{$adr->latitude}} / 
                          Longitude: {{$adr->longitude}}
                          <a href="{{URL('map2', $adr->id)}}" class="btn btn-link">
                            <i class="fas fa-map"></i> Ver no Mapa
                          </a>                         
                          <br>                          
                          ---------------------------------------------------------------------------------------------------------- <br> 
                          @endforeach
                      </div>
                    </div>
                    <br>
                    <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-users"></i> PESQUISADORES DESTA INSTITUIÇÃO</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                            @foreach($establishment->researchers as $res)
                            <a href="{{ URL::route('r.show',$res->id) }}">   
                            {{$res->name}}<br>
                            </a>
                            @endforeach
                      </div>
                    </div>                 
    <br>

    <!--Descrição da Instituição-->    
          
          <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="1">Descrição de {{$establishment->name}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  {{$establishment->description}}<br>
                  <br>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
              </div>
            </div>
          </div>
          

    <a class="btn btn-primary" href="{{URL::route('e.index')}}"><i class="fas fa-bars"></i> Instituições</a>
    
</div>   
 </div>
   </div>
    </div>                          
     </div>
       </div>
        </div>
        </div>
         </div>

@endsection