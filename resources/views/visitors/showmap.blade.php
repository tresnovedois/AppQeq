<!DOCTYPE html>

<html>

<head>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAGRQ4TNUMtY9wN-aP6_6vErdNXqfKphc"></script>
<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<style type="text/css">
    #map {
      border:1px solid;
      width: auto;
      height: 680px;

    }

    .map-title{
      color: white;
      font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
      font-size: 50px;
      margin-left: 60px;
      margin-top: 0
    }

    html, body {
      background: linear-gradient(to top, #7CB9E8, #f7f5f5);
      height: 100vh;
      background-attachment: fixed;
    }
</style>

</head>
<body>
 <div id="map">
  <script type="text/javascript">

   var locations = <?php print_r(json_encode($locations))?>;
   var l_lat = <?php print_r(json_encode($locations[0]->latitude))?>;
   var l_lng = <?php print_r(json_encode($locations[0]->longitude))?>;

    var mymap = new GMaps({
      el: '#map',
      lat: l_lat,
      lng: l_lng,
      zoom:17
    });

   $.each( locations, function( index, value ){
	  mymap.addMarker({
      lat: value.latitude,
      lng: value.longitude,
      title: value.city
    });

   });

  </script>
</div>

<div class="map-title">
      <a class="navbar-brand"  style="color: white; font-weight:bold; font-size:20px" href="{{ URL::previous() }}">
        <i class="fas fa-arrow-alt-circle-left"></i> {{$establishment[0]->name}}
      </a>
  </div>

</body>


</html>

