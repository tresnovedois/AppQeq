<!--ESTABLISHMENTS SHOW-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.5%">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                  <div class="card-header">
                    @if ($establishment->url_logo)
                          <img src="{{ URL($establishment->url_logo) }}" class="p-img">
                      @else  
                          <img src="{{ URL('image/empty.png') }}" class="p-img">
                      @endif 
                    Dados de <a style="font-weight:bold">{{$establishment->name}}</a></div>                  
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
    
                        <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-building"></i> DADOS DA EMPRESA</div>
                        <div class="card" style="border-radius: 0%">
                        <div class="row">
                            <div class="group col-md-4">
                            <div class="boxes">
                            NOME: {{$establishment->name}}<br>
                            EMAIL: {{$establishment->email}}<br>
                            CNPJ: {{$establishment->national_id}}                                                          
                            </div>                         
                        </div>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#1">
                          <i class="fas fa-info"></i> Descrição
                        </button>
                        <div style="margin-left:40%; margin-top:2%"><a class="btn btn-warning" href="/establishments/{{$establishment['id']}}/edit">
                          <i class="far fa-edit"></i> Editar</a></div>
                      </div> 
                      </div>

                      <br>

                      {{-- CADASTRO DE FOTO --}}

                <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-camera-retro"></i> Foto</div>
                <div class="card" style="border-radius: 0%">
                                
                  <div class="row">
                  <div class="col-md-2" style="margin:10px">
                
                    @if ($establishment->url_logo)
                        
                      <img src="{{ URL($establishment->url_logo) }}" style="width:150px;  height: auto">
  
                    @else  
                      <img src="{{ URL('image/empty.png') }}" class="p-img" style="width:150px;  height: auto">
                    @endif
                        
                    </div>
                
                    <div class="col-md-4">
                    
                      {{Form::open(array('route' => 'imagePostEstablishment','enctype' => 'multipart/form-data'))}}
                      {!! Form::file('image', array('class' => 'image', 'style' => 'display:none', 'id' => 'file')) !!}
                      {{-- envia o id para a função --}}
                      <input type="text" 
                        class="form-control"
                        value="{{$establishment->id}}" 
                        name="id"
                        style="display:none"
                        required
                        />
                      {{-- envia o id para a função --}}
                      
                      <input class="btn btn-link" type="button" id="image" value="Buscar imagem" onclick="document.getElementById('file').click();"/>
                      <i class="fas fa-search" style="color:blue"></i><br>
                      
                      <button type="submit" class="btn btn-link" style="color:green">
                        Enviar
                      </button><i class="fas fa-share-square" style="color:green"></i>
                      {!! Form::close() !!}
                    

                    <form method="post" action="{{ route('e.deletephoto' , [$establishment->id]) }}">

                    {{ csrf_field() }}
              
                    <input type="hidden" name="_method" value="put">

                    <input type="text" 
                           class="form-control"
                           value="{{$establishment->id}}" 
                           name="id"
                           style="display:none"
                           required
                    />

                    <input  type="text" 
                            style="display:none"
                            value=""
                            id="establishment-url-logo"
                            name="name"                                   
                            />

                    <input  class="btn btn-link"
                            value="Excluir"
                            onclick= "return confirm('Tem certeza de que deseja excluir a foto atual?')"
                            style="margin-left:1.3px; color:red"                            
                            type="submit"
                            /><i class="fas fa-times" style="color:red"></i>
                    </form>
                    </div>                                          
                    
                    @if ($message = Session::get('success'))
                      <div class="alert alert-success alert-block col-md-4" style="height:50px; margin-top:2px">
                        <button type="button" class="close" data-dismiss="alert">x</button>	
                          Imagem atualizada!
                      </div>
                    @endif    
                                                  
                    </div>
                    </div>
         
                {{-- FIM CADASTRO DE FOTO --}}

                <br>
    
                      <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-phone"></i> TELEFONES</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                          @foreach($establishment->estPhones as $est_phone)
                          <form method="post" action="{{ route('e.deletep') }}"> 
                          +{{$est_phone->ddi}}
                          ({{$est_phone->ddd}})
                          {{$est_phone->phone}}
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value = "{{$est_phone->id}}">
                          <button type="submit" class="btn btn-link" style="font-size:70%; color:red" 
                          onclick="return confirm('Tem certeza de que deseja excluir o número: +{{$est_phone->ddi}} ({{$est_phone->ddd}}) {{$est_phone->phone}}?')">
                          <i class="fas fa-times"></i>
                          </button>
                          <br>                                 
                          </form>   
                          @endforeach    
                      </div>
                    </div>
                      <br>
                    <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-location-arrow"></i> ENDEREÇOS</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                          @foreach($establishment->adrEstablishments as $adr)
                          <form method="post" action="{{ route('e.deletea') }}">
                          Endereço: {{$adr->address}}<br>
                          CEP: {{$adr->postal_code}}<br>
                          Cidade: {{$adr->city}}-{{$adr->state}}<br>
                          {{$adr->country}}<br>
                          Latitude: {{$adr->latitude}} / 
                          Longitude: {{$adr->longitude}}
                          <a href="{{URL('map', $adr->id)}}" class="btn btn-link">
                            <i class="fas fa-map"></i> Ver no Mapa
                          </a>                         
                          <br>                          
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="id" value = "{{$adr->id}}">
                          <button type="submit" class="btn btn-link" style="color:red" 
                          onclick="return confirm('Tem certeza de que deseja excluir {{$adr->address}}?')">
                          <i class="fas fa-trash-alt"></i> Excluir
                          </button>
                          <a href="{{route('e.showeditaddress', $adr->id)}}" class="btn btn-link">
                            <i class="far fa-edit"></i> Editar
                          </a>                               
                          </form>
                          {{-- <form method="post" action="{{ route('show.map') }}">
                          <input type="text" 
                                 class="form-control"
                                 value="{{$adr->id}}"
                                 name="id"
                                 style="display:none"
                                />
                          </form> --}}
                          ---------------------------------------------------------------------------------------------------------- <br> 
                          @endforeach
                      </div>
                    </div>
                    <br>
                    <div class="card-header" style="font-weight:bold; font-size:small"><i class="fas fa-users"></i> PESQUISADORES DESTA INSTITUIÇÃO</div>
                      <div class="card" style="border-radius: 0%">
                        <div class="boxes">
                            @foreach($establishment->researchers as $res)
                            <a href="/researchers/{{$res->id}}">   
                            {{$res->name}}<br>
                            </a>
                            @endforeach
                      </div>
                    </div>                 
    <br>

    <!--Descrição da Instituição-->    
          
          <div class="modal fade" id="1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="1">Descrição de {{$establishment->name}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  {{$establishment->description}}<br>
                  <br>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
              </div>
            </div>
          </div>
          
<!--Adicionar Telefones a instituição-->
<div class="modal fade" id="3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="1">Novo endereço de {{$establishment->name}}</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
           </button>
        </div>
        
        <div class="modal-body">
        <form method="post" action="{{ route('e.newphone') }}">
          <input type="hidden" name="_method" value="PUT" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}" >
          <input type="hidden" name="id" value = "{{$establishment->id}}">
  
          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="establishment-ddi">DDI*</label>
              <input type="text" 
                      class="form-control" 
                      id="establishment-ddi" 
                      placeholder="Ex:55" 
                      name="ddi"
                      maxlength="4"
                      required
                      />
            </div>  
  
            <div class="form-group col-md-2">
              <label for="establishment-ddd">DDD*</label>
              <input type="text" 
                      class="form-control" 
                      id="establishment-ddd" 
                      placeholder="Ex:61" 
                      name="ddd"
                      maxlength="2"
                      required
                      />
            </div>  
  
            <div class="form-group col-md-3">
              <label for="establishment-phone">Telefone*</label>
              <input type="text" 
                      class="form-control" 
                      id="establishment-phone" 
                      placeholder="Apenas números!" 
                      name="phone"
                      maxlength="10"
                      required
                      />
            </div>
         </div>
        <button type="submit" class="btn btn-success"> Adicionar</button>
  </form>
 </div>
 </div>
</div>
  </div>
    
    <!--Adicionar Endereços a instituição-->
    <div class="modal fade" id="2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
       <div class="modal-content">
        <div class="modal-header">
         <h5 class="modal-title" id="1">Adicionar endereço em {{$establishment->name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
         </div>
          <div class="modal-body">
            <form method="post" action="{{ route('e.newad') }}">
              <input type="hidden" name="_method" value="PUT" >
              <input type="hidden" name="_token" value="{{ csrf_token() }}" >
              <input type="hidden" name="id" value = "{{$establishment->id}}">

              <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="establishment-zip">CEP*</label>
                    <input type="text" 
                            class="form-control" 
                            id="establishment-zip" 
                            placeholder="Apenas números!" 
                            name="postal_code"
                            maxlength="20"
                            required
                            />
                </div>

                <div class="form-group col-md-4">
                  <label for="establishment-address">Endereço*</label>
                  <input type="text" 
                          class="form-control" 
                          id="establishment-address" 
                          placeholder="Ex: Rua das Artes, Bairro Ipê casa 32" 
                          name="address"
                          maxlength="90"
                          required
                          />
                </div>

                <div class="form-group col-md-2">
                  <label for="establishment-city">Cidade*</label>
                  <input type="text" 
                          class="form-control" 
                          id="establishment-city" 
                          placeholder="Ex:Brasília" 
                          name="city"
                          maxlength="50"
                          required
                          />
                </div>

                <div class="form-group col-md-1">
                  <label for="establishment-state">UF*</label>
                  <input type="text" 
                  class="form-control" 
                  id="establishment-state" 
                  placeholder="Ex: DF" 
                  name="state"
                  maxlength="2"
                  required
                  />
                </div>   

                <div class="form-group col-md-3">
                  <label for="establishment-country">País*</label>
                  <input type="text" 
                  class="form-control" 
                  id="establishment-country" 
                  placeholder="Brasil" 
                  name="country"
                  maxlength="45"
                  required
                  />
                </div>

                <div class="form-row">

                <div class="form-group col-md-6">
                  <label for="establishment-latitude">Latitude</label>
                  <input type="text" 
                          class="form-control" 
                          id="establishment-latitude" 
                          placeholder="Ex: -15.822395" 
                          name="latitude"
                          
                          />
                </div>

                <div class="form-group col-md-6">
                  <label for="establishment-longitude">Longitude</label>
                  <input type="text" 
                          class="form-control" 
                          id="establishment-longitude" 
                          placeholder="Ex: 15.822395" 
                          name="longitude"
                          
                          />
              </div>
               </div>
                 </div>
                  <button type="submit" class="btn btn-success"> Adicionar</button>
              </form>
            </div>
            </div>
          </div>
        </div>
     

  <!--botões-->
  <div> 
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#2">
      <i class="fas fa-location-arrow"></i> Novo Endereço
    </button> 
    
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#3">
      <i class="fas fa-phone"></i> Novo Telefone
    </button>

  </div>
          
<!--deletar instituição-->     
  <br>
  <div style="margin-left:79.4%">    
  <form method="POST">
    <a class="btn btn-primary" href="/establishments"><i class="fas fa-bars"></i> Instituições</a>
    <input type="hidden" name="_method" value="DELETE" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
    <input type="hidden" name="id" value = "{{$establishment['id']}}">
    <input type="hidden" name="form_type" value = "deleteRes">
    <input
        onclick="return confirm('Deletar TODOS os registros de {{$establishment->name}}?');"
        class="btn btn-danger"
        value="Deletar"
        style="cursor: pointer;"
        type="submit"
    >
</form>
</div>   
 </div>
   </div>
    </div>                          
     </div>
       </div>
        </div>
        </div>
         </div>

@endsection