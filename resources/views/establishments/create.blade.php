<!--ESTABLISHMENTS CREATE-->
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                    <div class="card-header"><a style="font-weight:bold">NOVA INSTITUIÇÃO</a></div>     
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <ul class="list-group"></ul>  
                        <li class="list-group-item">
                        <form method="post" action="{{ route('establishments.store') }}">

                          {{ csrf_field() }}
                    
                          
                          <div class="form-row">

                          <div class="form-group col-md-9">
                          <label for="establishment-name">Nome da Instituição*</label>
                          <input type="text" 
                                 class="form-control" 
                                 id="establishment-name" 
                                 placeholder="Nome Completo da Instituição" 
                                 name="name"
                                 maxlength="60"
                                 required
                                 />
                          </div>

                          <div class="form-group col-md-3">
                            <label for="establishment-id">CNPJ*</label>
                            <input type="text" 
                                   class="form-control" 
                                   id="establishment-id" 
                                   placeholder="Apenas números!" 
                                   name="national_id"
                                   maxlength="20"
                                   required
                                   />
                            </div>
                          </div>

                          <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="establishment-email">Email*</label>
                            <input type="email" 
                                    class="form-control" 
                                    id="establishment-email" 
                                    placeholder="exemplo@email.com"
                                    name ="email"
                                    maxlength="45"
                                    required/>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="establishment-ddi">DDI*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="establishment-ddi" 
                                    placeholder="Ex:55" 
                                    name="ddi"
                                    maxlength="4"
                                    required
                                    />
                        </div>  

                        <div class="form-group col-md-1">
                            <label for="establishment-ddd">DDD*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="establishment-ddd" 
                                    placeholder="Ex:61" 
                                    name="ddd"
                                    maxlength="2"
                                    required
                                    />
                        </div>  

                        <div class="form-group col-md-3">
                            <label for="establishment-phone">Telefone*</label>
                            <input type="text" 
                                    class="form-control" 
                                    id="establishment-phone" 
                                    placeholder="Apenas números!" 
                                    name="phone"
                                    maxlength="10"
                                    required
                                    />
                        </div> 
                        
                        
                        <div class="form-group col-md-3">
                            <label for="establishment-country">País*</label>
                            <input type="text" 
                            class="form-control" 
                            id="establishment-country" 
                            placeholder="Brasil" 
                            name="country"
                            maxlength="45"
                            required
                            />
                        </div>
                        </div>
                         
                        Descrição: <br>
                          <div class="input-group">      
                              <textarea class="form-control" 
                                        aria-label="With textarea"
                                        name="description"
                              ></textarea>
                            </div>
                            <br>

                            <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="establishment-zip">CEP*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="establishment-zip" 
                                        placeholder="Apenas números!" 
                                        name="postal_code"
                                        maxlength="20"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-7">
                                <label for="establishment-address">Endereço*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="establishment-address" 
                                        placeholder="Ex: Rua das Artes, Bairro Ipê casa 32" 
                                        name="address"
                                        maxlength="90"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-2">
                                <label for="establishment-city">Cidade*</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="establishment-city" 
                                        placeholder="Ex:Brasília" 
                                        name="city"
                                        maxlength="50"
                                        required
                                        />
                            </div>

                            <div class="form-group col-md-1">
                                <label for="establishment-state">UF*</label>
                                <input type="text" 
                                class="form-control" 
                                id="establishment-state" 
                                placeholder="Ex: DF" 
                                name="state"
                                maxlength="2"
                                required
                                />
                                </div>   
                            </div>  

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="establishment-latitude">Latitude</label>
                                    <input type="text" 
                                            class="form-control" 
                                            id="establishment-latitude" 
                                            placeholder="Ex: -15.822395" 
                                            name="latitude"
                                            
                                            />
                                </div>

                            <div class="form-group col-md-6">
                                <label for="establishment-longitude">Longitude</label>
                                <input type="text" 
                                        class="form-control" 
                                        id="establishment-longitude" 
                                        placeholder="Ex: 15.822395" 
                                        name="longitude"
                                        
                                        />
                            </div>
                            </div>
                            <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>
                            <br>
                          <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Salvar</button>
                        <a class="btn btn-primary" href="{{ route('home') }}"><i class="fas fa-times"></i> Cancelar</a>
                  </form>                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection