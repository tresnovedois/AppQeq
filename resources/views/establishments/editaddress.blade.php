{{-- ESTABLISHMENT ADDRESS --}}
@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
  <div class="row justify-content-center">
   <div class="col-md-10">
    <div class="card">
     <div class="card-header">Editar Endereço</a></div>                  
      <div class="card-body">
        @if (session('status'))
         <div class="alert alert-success">
          {{ session('status') }}
         </div>
         @endif 

         <form method="post" action="{{ route('e.editaddress', [$adr->id]) }}">

            {{ csrf_field() }}
      
        <input type="hidden" name="_method" value="put">

         <div class="form-row">
           <div class="form-group col-md-2">
            <label for="establishment-zip">CEP*</label>
            <input type="text" 
                    class="form-control" 
                    id="establishment-zip" 
                    value="{{$adr->postal_code}}" 
                    name="postal_code"
                    required
                    />
            </div>

            <div class="form-group col-md-7">
              <label for="establishment-address">Endereço*</label>
              <input type="text" 
                     class="form-control" 
                     id="establishment-address" 
                     value="{{$adr->address}}" 
                     name="address"
                     required
                    />
            </div>

            <div class="form-group col-md-2">
              <label for="establishment-city">Cidade*</label>
              <input type="text" 
                     class="form-control" 
                     id="establishment-city" 
                     value="{{$adr->city}}" 
                     name="city"
                     required
                     />
            </div>

            <div class="form-group col-md-1">
             <label for="establishment-state">UF*</label>
             <input type="text" 
                   class="form-control" 
                   id="establishment-state" 
                   value="{{$adr->state}}" 
                   name="state"
                   required
                   />
                </div>   
            </div>  

            <div class="form-row">
              <div class="form-group col-md-4">
              <label for="establishment-latitude">Latitude</label>
              <input type="text" 
                     class="form-control" 
                     id="establishment-latitude" 
                     value="{{$adr->latitude}}" 
                     name="latitude"  
                    />
              </div>

            <div class="form-group col-md-4">
              <label for="establishment-longitude">Longitude</label>
              <input type="text" 
                     class="form-control" 
                     id="establishment-longitude" 
                     value="{{$adr->longitude}}" 
                     name="longitude"       
                    />
              </div>

              <div class="form-group col-md-4">
                <label for="establishment-country">País*</label>
                <input type="text" 
                       class="form-control" 
                       id="establishment-country" 
                       value="{{$adr->country}}" 
                       name="country"
                       required
                       />
              </div>

              <input type="text" 
                     class="form-control"
                     value="{{$adr->establishment_id}}" 
                     name="establishment_id"
                     style="display:none"
                     required
                     />

            </div>

<a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>

        <div style="margin-left:80%">
            <a class="btn btn-outline-danger" href="{{ URL::previous() }}">Cancelar</a>
            <input type="submit" class="btn btn-outline-success"  value="Salvar"/>
        </div>

        </form> 
</div>
</div>
</div>
</div>
</div>

@endsection