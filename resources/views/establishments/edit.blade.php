<!--ESTABLISHMENTS EDIT-->

@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 3.8%">
   <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
      <div class="card-header">Editar dados de <a style="font-weight:bold">{{$establishment->name}}</a></div>                  
          <div class="card-body">
              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
              <ul class="list-group"></ul>  
              <li class="list-group-item">
                  <form method="post" action="{{ route('establishments.update', [$establishment->id]) }}">

                    {{ csrf_field() }}
              
                    <input type="hidden" name="_method" value="put">

                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <label for="establishment-name">Nome*</label>
                          <input required 
                                  type="text" 
                                  class="form-control" 
                                  id="establishment-name" 
                                  value="{{ $establishment->name}}"
                                  name="name"  
                                  maxlength="60"                                 
                                  />
                          </div>
                        
                      </div>
                        
                        <div class="form-row">

                          <div class="form-group col-md-6">
                          <label for="establishment-id">CNPJ*</label>
                          <input type="text" 
                                 class="form-control" 
                                 id="establishment-id" 
                                 value="{{ $establishment->national_id}}" 
                                 name="national_id"
                                 maxlength="20"
                                 required
                                 />
                          </div> 
                          
                          <div class="form-group col-md-6">
                            <label for="establishment-email">Email*</label>
                            <input required
                                    type="email" 
                                    class="form-control" 
                                    id="establishment-email" 
                                    value="{{ $establishment->email}}"
                                    name="email"
                                    maxlength="45"
                                    />
                          </div>

                          </div>  

                          <div class="form-row">

                           Descrição: <br>
                          <div class="input-group">      
                              <textarea class="form-control" 
                                        aria-label="With textarea"
                                        name="description"
                          >{{$establishment->description}}</textarea>
                            </div>
                            <br>
                            
  <a style="margin-left:1%"><i>Favor preencher <b>corretamente</b> todos os campos que possuam (*)!</i></a>
  <div style="margin-left:80%">
  <a class="btn btn-outline-danger" href="/establishments/{{$establishment->id}}">Cancelar</a>
  <input type="submit" class="btn btn-outline-success"  value="Salvar"/>

  </div>
                    <!--FIM DO FORMULÁRIO-->
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

