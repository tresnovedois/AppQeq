<!--ESTABLISHMENTS INDEX-->
@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 3.5%">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">               
        <div class="card-header"><i class="fas fa-university"></i> Banco de Instituições</div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success">
              {{ session('status') }}
              </div>
            @endif   

            <a class="btn btn-primary" href="/establishments/create"><i class="fas fa-plus"></i> Nova Instituição</a> <br>

            @foreach($establishments as $establishment)
            <br>
            <li class="list-group-item">
            @if ($establishment->url_logo)
            <a href="/establishments/{{ $establishment->id }}">
                <img src="{{ URL($establishment->url_logo) }}" class="p-img">
            </a>
            @else  
            <a href="/establishments/{{ $establishment->id }}">
              <img src="{{ URL('image/empty.png') }}" class="p-img">
            </a>
            @endif 
            <a href="/establishments/{{ $establishment->id }}">  
            {{ $establishment->name }}
            </a>                           
            @endforeach 
            </li>                   
            <br>
             
</div>     
</div>
</div>
</div>
</div>

@endsection