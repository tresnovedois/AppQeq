<?php

Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

//VISITORS

Route::get('home2', 'VisitorsController@homeVisitors')->name('v.home');

        //RESEARCHERS
        Route::get('home2/researchers', 'VisitorsController@indexResearchers')->name('r.index');
        Route::get('home2/researchers/{id}', 'VisitorsController@showResearchers')->name('r.show');

        //LOTS
        Route::get('home2/lots', 'VisitorsController@indexLots')->name('l.index');
        Route::get('home2/lots/{id}', 'VisitorsController@showLots')->name('l.show');

        //ESTABLISHMENTS
        Route::get('home2/establishments', 'VisitorsController@indexEstablishments')->name('e.index');
        Route::get('home2/establishments/{id}', 'VisitorsController@showEstablishments')->name('e.show');
        Route::get('map2/{id}', 'EstablishmentsController@showMapVisitor');

//GEOLOCATION
Route::get('map/{id}', 'EstablishmentsController@showMap')->middleware('auth');


//IMAGES
Route::post('imagePostEstablishment', 'ImageController@imagePostEstablishments')->middleware('auth')->name('imagePostEstablishment');

Route::post('imagePostResearcher', 'ImageController@imagePostResearchers')->middleware('auth')->name('imagePostResearcher');


//HOME
Route::resource('home', 'HomeController')->middleware('auth');

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');


//RESEARCHERS
Route::put('researchers/newphone', 'ResearchersController@newPhone')->middleware('auth')->name('r.newphone');

Route::put('researchers/newadr', 'ResearchersController@newAddr')->middleware('auth')->name('r.newad');

Route::put('researchers/link', 'ResearchersController@link')->middleware('auth')->name('r.link');

Route::put('researchers/tolot', 'ResearchersController@toLot')->middleware('auth')->name('r.lot');

Route::delete('researchers/unlink', 'ResearchersController@unlink')->middleware('auth')->name('unlink');

Route::delete('researchers/deletelot', 'ResearchersController@deleteLot')->middleware('auth')->name('deleteLot');

Route::put('researchers/deletephoto', 'ResearchersController@deletePhoto')->middleware('auth')->name('r.deletephoto');

Route::delete('researchers/deletephone', 'ResearchersController@deletePhone')->middleware('auth')->name('r.deletep');

Route::delete('researchers/deleteaddress', 'ResearchersController@deleteAddress')->middleware('auth')->name('r.deletea');

Route::get('researchers/editaddress/{id}', 'ResearchersController@showEditAddress')->middleware('auth')->name('r.showeditaddress');

Route::put('researchers/{id}/editaddress', 'ResearchersController@editAddress')->middleware('auth')->name('r.editaddress');

Route::resource('researchers', 'ResearchersController')->middleware('auth');


//ESTABLISHMENTS
Route::put('establishments/newphone', 'EstablishmentsController@newPhone')->middleware('auth')->name('e.newphone');

Route::put('establishments/newaddress', 'EstablishmentsController@newAddr')->middleware('auth')->name('e.newad');

Route::delete('establishments/deletephone', 'EstablishmentsController@deletePhone')->middleware('auth')->name('e.deletep');

Route::delete('establishments/deleteaddress', 'EstablishmentsController@deleteAddress')->middleware('auth')->name('e.deletea');

Route::get('establishments/editaddress/{id}', 'EstablishmentsController@showEditAddress')->middleware('auth')->name('e.showeditaddress');

Route::put('establishments/{id}/editaddress', 'EstablishmentsController@editAddress')->middleware('auth')->name('e.editaddress');

Route::put('establishments/deletephoto', 'EstablishmentsController@deletePhoto')->middleware('auth')->name('e.deletephoto');

Route::resource('establishments', 'EstablishmentsController')->middleware('auth');

//WORK LOTS

Route::resource('lots', 'LotsController')->middleware('auth');













































// Route::resource('adr_researcher', 'AdrResearcherController')->middleware('auth');

// Route::resource('adr_establishments', 'AdrEstablishmentsController')->middleware('auth');
// Route::resource('est_phone', 'EstPhoneController')->middleware('auth');

// 
// Route::get('researchers/', 'ResearchersController@index')->middleware('auth');
// Route::get('researchers/{id}', 'ResearchersController@show')->middleware('auth');
// Route::put('researchers', 'ResearchersController@edit')->middleware('auth');
// Route::delete('delete/{id}',array('uses' => 'ResearchersController@destroy', 'as' => 'deleteroute'))->middleware('auth');




